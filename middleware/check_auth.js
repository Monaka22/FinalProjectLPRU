const alert = require('alert-node');

module.exports = (req, res, next) => {
        if(req.session.isLoggedIn != null  && req.session.isLoggedIn == true){
            next();
        }
        else{
            alert("Please login");
            res.redirect('/main');
        }
};