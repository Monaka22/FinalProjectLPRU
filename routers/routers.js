const router = require('express').Router();
const session = require('express-session');
const alert = require('alert-node');

const dashController = require('../controllers/dashController');
const branchController = require('../controllers/branchController');
const positionController = require('../controllers/positionController');
const empController = require('../controllers/empController');
const projectController = require('../controllers/projectController');
const benefitController = require('../controllers/benefitController');
const positionEmpController = require('../controllers/positionEmpController');
const notfound = require('../controllers/404notfond');
const checkAuth = require('../middleware/check_auth');
const hrdashController = require('../controllers/hrdashController');
const hrbranchController = require('../controllers/hrbranchController');
const hrempController = require('../controllers/hrempController');
const hrbenefitController = require('../controllers/hrbenefitController');
const hrpositionEmpController = require('../controllers/hrpositionEmpController');

router.use(session({
    secret: 'HRproject',
    cookie: {
        maxAge: 14400000
    },
    resave: true,
    saveUninitialized: false
}));
//admin route
router.get('/dash', checkAuth, dashController.list);
router.get('/dash2', checkAuth, dashController.list2);
router.get('/dash3', checkAuth, dashController.list3);
router.get('/dash4', checkAuth, dashController.list4);
router.get('/dash41', checkAuth, dashController.list41);
router.get('/branch', checkAuth, branchController.list);
router.get('/branchadd', checkAuth, branchController.addform);
router.post('/branch', checkAuth, branchController.save);
router.get('/branchedit', checkAuth, branchController.editform);
router.post('/branchedit', checkAuth, branchController.update);
router.get('/branchdelete', checkAuth, branchController.delete);
router.get('/position', checkAuth, positionController.list);
router.get('/positionadd', checkAuth, positionController.addform);
router.post('/position', checkAuth, positionController.save);
router.get('/positionedit', checkAuth, positionController.editform);
router.post('/positionedit', checkAuth, positionController.update);
router.get('/positiondelete', checkAuth, positionController.delete);
router.get('/project', checkAuth, projectController.list);
router.get('/projectadd', checkAuth, projectController.addform);
router.post('/project', checkAuth, projectController.save);
router.get('/projectfrom', checkAuth, projectController.projectform);
router.get('/projectedit', checkAuth, projectController.editform);
router.post('/projectedit', checkAuth, projectController.update);
router.get('/projectdelete', checkAuth, projectController.delete);
router.get('/emp', checkAuth, empController.list);
router.get('/empadd', checkAuth, empController.addform);
router.post('/emp', checkAuth, empController.save);
router.get('/empedit', checkAuth, empController.editform);
router.post('/empedit', checkAuth, empController.update);
router.get('/empdelete', checkAuth, empController.delete);
router.get('/benefit', checkAuth, benefitController.list);
router.get('/benefitadd', checkAuth, benefitController.addform);
router.post('/benefit', checkAuth, benefitController.save);
router.get('/benefitedit', checkAuth, benefitController.editform);
router.post('/benefitedit', checkAuth, benefitController.update);
router.get('/benefitdelete', checkAuth, benefitController.delete);
router.get('/position_emp', checkAuth, positionEmpController.list);
router.get('/position_empadd', checkAuth, positionEmpController.addform);
router.post('/position_emp', checkAuth, positionEmpController.save);
router.get('/position_empdelete', checkAuth, positionEmpController.delete);
router.get('/projectaddit', checkAuth, projectController.projectaddit);
router.get('/projectadditadd', checkAuth, projectController.projectadditaddform);
router.post('/projectaddit', checkAuth, projectController.projectadditsave);
router.get('/projectadditdelete', checkAuth, projectController.projectadditdelete);
router.get('/branchaddit', checkAuth, branchController.branchaddit);
router.get('/branchadditadd', checkAuth, branchController.branchadditaddform);
router.post('/branchadditadd', checkAuth, branchController.branchadditsave);
router.get('/branchadditedit', checkAuth, branchController.branchadditeditform);
router.post('/branchadditedit', checkAuth, branchController.branchadditeditsave);
router.get('/fixcostadd', checkAuth, branchController.fixcostaddform);
router.post('/fixcostadd', checkAuth, branchController.fixcostsave);
router.get('/fixcostedit', checkAuth, branchController.fixcosteditform);
router.post('/fixcostedit', checkAuth, branchController.fixcosteditsave);
router.get('/branchadditdelete', checkAuth, branchController.additdelete);
router.get('/fixcostdelete', checkAuth, branchController.fixcostdelete);
router.get('/projectteam', checkAuth, projectController.projectteam);
router.get('/teamadd', checkAuth, projectController.teamadd);
router.post('/teamfreetime', checkAuth, projectController.teamfreetime);
router.get('/teamsave', checkAuth, projectController.teamsave);
router.get('/teamdelete', checkAuth, projectController.teamdelete);

//hr route
router.get('/hrdash', checkAuth, hrdashController.list);
router.get('/hrdash2', checkAuth, hrdashController.list2);
router.get('/hrdash3', checkAuth, hrdashController.list3);
router.get('/hrdash4', checkAuth, hrdashController.list4);
router.get('/hrdash41', checkAuth, hrdashController.list41);
router.get('/hrbranch', checkAuth, hrbranchController.list);
router.get('/hrbranchadd', checkAuth, hrbranchController.addform);
router.post('/hrbranch', checkAuth, hrbranchController.save);
router.get('/hrbranchedit', checkAuth, hrbranchController.editform);
router.post('/hrbranchedit', checkAuth, hrbranchController.update);
router.get('/hrbranchdelete', checkAuth, hrbranchController.delete);
router.get('/hremp', checkAuth, hrempController.list);
router.get('/hrempadd', checkAuth, hrempController.addform);
router.post('/hremp', checkAuth, hrempController.save);
router.get('/hrempedit', checkAuth, hrempController.editform);
router.post('/hrempedit', checkAuth, hrempController.update);
router.get('/hrempdelete', checkAuth, hrempController.delete);
router.get('/hrbenefit', checkAuth, hrbenefitController.list);
router.get('/hrbenefitadd', checkAuth, hrbenefitController.addform);
router.post('/hrbenefit', checkAuth, hrbenefitController.save);
router.get('/hrbenefitedit', checkAuth, hrbenefitController.editform);
router.post('/hrbenefitedit', checkAuth, hrbenefitController.update);
router.get('/hrbenefitdelete', checkAuth, hrbenefitController.delete);
router.get('/hrposition_emp', checkAuth, hrpositionEmpController.list);
router.get('/hrposition_empadd', checkAuth, hrpositionEmpController.addform);
router.post('/hrposition_emp', checkAuth, hrpositionEmpController.save);
router.get('/hrposition_empdelete', checkAuth, hrpositionEmpController.delete);
router.get('/hrbranchaddit', checkAuth, hrbranchController.branchaddit);
router.get('/hrbranchadditadd', checkAuth, hrbranchController.branchadditaddform);
router.post('/hrbranchadditadd', checkAuth, hrbranchController.branchadditsave);
router.get('/hrbranchadditedit', checkAuth, hrbranchController.branchadditeditform);
router.post('/hrbranchadditedit', checkAuth, hrbranchController.branchadditeditsave);
router.get('/hrfixcostadd', checkAuth, hrbranchController.fixcostaddform);
router.post('/hrfixcostadd', checkAuth, hrbranchController.fixcostsave);
router.get('/hrfixcostedit', checkAuth, hrbranchController.fixcosteditform);
router.post('/hrfixcostedit', checkAuth, hrbranchController.fixcosteditsave);
router.get('/hrbranchadditdelete', checkAuth, hrbranchController.additdelete);
router.get('/hrfixcostdelete', checkAuth, hrbranchController.fixcostdelete);












//other route
router.get('/notfound', notfound.list);
router.get('/dash', function (req, res) {
    res.render("dashboard");
});

router.get('/main', function (req, res) {
    res.render("login");
});

router.post('/login', function (req, res) {
    req.getConnection((err, conn) => {
        conn.query(`SELECT * FROM user`, (err, user) => {
            let j =0;
            for (let i = 0; i < user.length; i++) {
                if (req.body.username == user[i].username && req.body.password == user[i].password) {
                     j = 1;
            }
        }
            console.log(j)
            if (j == 1) {
                req.session.username = req.body.username;
                req.session.isLoggedIn = true;
                if(req.body.username =="admin"){
                    res.redirect('/dash');
                }else{
                    res.redirect('/hrdash')
                }
            } else {
                alert("Invalid username or password");
                res.redirect('/main')
            }
        });
    });
});
router.get('/',checkAuth, function (req, res) {
    res.redirect('/dash')
});
router.get('/profile', function (req, res) {
    res.render("profile");
});
router.get('/hrprofile', function (req, res) {
    res.render("0profile");
});
router.post('/profile', function (req, res) {
    req.getConnection((err, conn) => {
        conn.query(`UPDATE user SET password = '${req.body.password}' WHERE id = '${req.body.id}'`, (err, result) => {
            if (err) throw err;
            console.log(result.affectedRows + " record(s) updated");
            res.redirect('/dash')
        });
    });
});
router.post('/hrprofile', function (req, res) {
    req.getConnection((err, conn) => {
        conn.query(`UPDATE user SET password = '${req.body.password}' WHERE id = '${req.body.id}'`, (err, result) => {
            if (err) throw err;
            console.log(result.affectedRows + " record(s) updated");
            res.redirect('/hrdash')
        });
    });
});
router.get('/logout', function (req, res) {
    req.session.destroy();
    res.redirect('/main')
});


module.exports = router;