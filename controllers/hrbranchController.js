const controller = {};
// const bodyPar = require('../app');
// controller.use(bodyPar());
const moment = require('moment');
const alert = require('alert-node');
controller.list = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM branch Where status = 1', (err, branchs) => {
            if (err) {
                res.json(err);
            }
            res.render("0branchDatatable", { header: "สาขา", data: branchs });
        });
    });
};

controller.addform = (req,res) =>{
    res.render("0branchAdd");
}

controller.listById = (req, res) => {
    id = req.params.id;
    req.getConnection((err, conn) => {
        conn.query(`SELECT * FROM branch WHERE branch_id='${id}'`, (err, branchs) => {
            if (err) {
                res.json(err);
            }
            res.send(branchs);
        });
    });
};


controller.save = (req, res) => {
    req.getConnection((err, conn) => {
        const data = {
            addDate: Date.now(),
            updateDate: Date.now(),
            branch_name: req.body.branch_name,
            branch_address: req.body.branch_address,
            status: 1
        }
        conn.query(`SELECT * FROM branch Where branch_name = '${data.branch_name}' AND status = 1`, (err, branch) => {
            if(branch.length == 0){
                console.log(data);
                const sql = `INSERT INTO branch  VALUES ('${data.addDate}','${data.updateDate}','${null}','${data.branch_name}','${data.branch_address}','${data.status}')`;
                conn.query(sql, function (err, result) {
                    if (err) throw err;
                    console.log("1 record inserted");
                    res.redirect('/hrbranch')
                });
            }else{
                alert("ข้อมูลซ้ำ");
                res.redirect('/hrbranchadd')
            }
        });
       
    });
};
controller.editform =(req,res)=>{
    id = req.query.id;
    req.getConnection((err, conn) => {
        conn.query(`SELECT * FROM branch Where branch_id = '${id}'`, (err, branchs) => {
            if (err) {
                res.json(err);
            }
            console.log(branchs)
            res.render("0branchEdit", { data: branchs });
        });
    });
    
}

controller.update = (req, res) => {
    req.getConnection((err, conn) => {

        const data = {
            branch_name: req.body.branch_name,
            branch_address: req.body.branch_address,
            updateDate: Date.now(),
            id: req.body.branch_id
        }
        console.log("update: " + JSON.stringify(data))
        sql = `UPDATE branch SET updatedAt = '${data.updateDate}',branch_name = '${data.branch_name}',branch_address = '${data.branch_address}' WHERE branch_id = '${data.id}'`;
        conn.query(sql, function (err, result) {
            if (err) throw err;
            console.log(result.affectedRows + " record(s) updated");
            res.redirect('/hrbranch')
        });
    });
};

controller.delete = (req, res) => {
    req.getConnection((err, conn) => {
        id = req.query.id;
        console.log(id);
        sql = `DELETE FROM branch WHERE branch_id='${id}'`;
        conn.query(sql, function (err, result) {
            if (err) throw err;
            console.log("1 delete inserted");
            res.redirect('/hrbranch')
        });
    });
}

controller.branchaddit= (req, res) => {
    let id = req.query.id;
    req.getConnection((err, conn) => {
        conn.query(`SELECT * FROM branchaddit Where branch_addit_branch_id = '${id}'`, (err, addit) => {
            if (err) {
                res.json(err);
            }
            req.getConnection((err, conn) => {
                conn.query(`SELECT * FROM fixcost Where fixcost_branch_id = '${id}'`, (err, fixcost) => {
                    if (err) {
                        res.json(err);
                    }req.getConnection((err, conn) => {
                        conn.query(`SELECT * FROM branch Where branch_id = '${id}'`, (err, branch) => {
                            res.render("0branchAdditDatatable", {
                                header: "ค่าใช้จ่าย",
                                data: addit,
                                data2:fixcost,
                                branch_name: branch[0].branch_name,
                                branch_id:branch[0].branch_id
                            });
                        });
                    });
                   
                });
            });
        });
    });
}
controller.branchadditaddform = (req, res) => {
    let id = req.query.id;
        res.render("0branchAdditAdd",{branch_id: id} )
}
controller.branchadditsave = (req, res) => {
    req.getConnection((err, conn) => {
        let date = req.body.branch_addit_date;
        date = date.split("-");
        let newdate = date[0] + "/" + date[1] + "/" + date[2];
        let numdate = parseDMY(newdate).getTime();
        const data = {
            addDate: Date.now(),
            updateDate: Date.now(),
            branch_addit_title: req.body.branch_addit_title,
            branch_addit_price: req.body.branch_addit_price,
            branch_addit_date: numdate,
            branch_addit_branch_id: req.body.branch_id
        }
        console.log(data);
        const sql = `INSERT INTO branchaddit  VALUES ('${data.addDate}','${data.updateDate}','${null}','${data.branch_addit_title}','${data.branch_addit_price}'
        ,'${data.branch_addit_date}','${data.branch_addit_branch_id}')`;
        conn.query(sql, function (err, result) {
            if (err) throw err;
            console.log("1 record inserted");
            res.redirect("/hrbranchaddit?id="+data.branch_addit_branch_id)
        });
    });
};

controller.fixcostaddform  = (req, res) => {
    let id = req.query.id;
    res.render("0FixcostAdd",{branch_id: id} )
}
controller.fixcostsave = (req, res) => {
    req.getConnection((err, conn) => {
        const data = {
            addDate: Date.now(),
            updateDate: Date.now(),
            fixcost_title: req.body.fixcost_title,
            fixcost_price: req.body.fixcost_price,
            fixcost_note: req.body.fixcost_note,
            fixcost_branch_id: req.body.branch_id
        }
        console.log(data);
        const sql = `INSERT INTO fixcost  VALUES ('${data.addDate}','${data.updateDate}','${null}','${data.fixcost_title}','${data.fixcost_price}'
        ,'${data.fixcost_note}','${data.fixcost_branch_id}')`;
        conn.query(sql, function (err, result) {
            if (err) throw err;
            console.log("1 record inserted");
            res.redirect("/hrbranchaddit?id="+data.fixcost_branch_id)
        });
    });
}
controller.additdelete = (req, res) => {
    req.getConnection((err, conn) => {
        id = req.query.id;
        branch_id = req.query.branch_id;
        console.log(id);
        sql = `DELETE FROM branchaddit WHERE branch_addit_id='${id}'`;
        conn.query(sql, function (err, result) {
            if (err) throw err;
            console.log("1 delete inserted");
            res.redirect("/hrbranchaddit?id="+branch_id)
        });
    });
}
controller.fixcostdelete = (req, res) => {
    req.getConnection((err, conn) => {
        id = req.query.id;
        branch_id = req.query.branch_id;
        console.log(id);
        sql = `DELETE FROM fixcost WHERE fixcost_id='${id}'`;
        conn.query(sql, function (err, result) {
            if (err) throw err;
            console.log("1 delete inserted");
            res.redirect("/hrbranchaddit?id="+branch_id)
        });
    });
}
controller.branchadditeditform = (req,res) =>{
    id = req.query.id;
    branch_id = req.query.branch_id;
    req.getConnection((err, conn) => {
        conn.query(`SELECT * FROM branchaddit Where branch_addit_id = '${id}'`, (err, branchs) => {
            if (err) {
                res.json(err);
            }
            let data = {branch_addit_id:branchs[0].branch_addit_id,branch_addit_title:branchs[0].branch_addit_title,branch_addit_price:branchs[0].branch_addit_price
                ,branch_addit_date:moment(branchs[0].branch_addit_date).format("DD/MM/YYYY") }
            console.log(branchs)
            res.render("0branchAdditEdit", { data: data , branch_id:branch_id});
        });
    });
}
controller.branchadditeditsave = (req,res) =>{
    req.getConnection((err, conn) => {
        let date = req.body.branch_addit_date;
        console.log(date)
       date = date.split("-");
        let newdate = date[0] + "/" + date[1] + "/" + date[2];
        let numdate = parseDMY(newdate).getTime();
        const data = {
            id:req.body.branch_addit_id,
            addDate: Date.now(),
            updateDate: Date.now(),
            branch_addit_title: req.body.branch_addit_title,
            branch_addit_price: req.body.branch_addit_price,
            branch_addit_date: numdate,
            branch_id: req.body.branch_id
        }
        console.log("update: " + JSON.stringify(data))
        sql = `UPDATE branchaddit SET updatedAt = '${data.updateDate}', branch_addit_title = '${data.branch_addit_title}'
        , branch_addit_price = '${data.branch_addit_price}', branch_addit_date = '${data.branch_addit_date}'
        WHERE branch_addit_id = '${data.id}'`;
        conn.query(sql, function (err, result) {
            if (err) throw err;
            console.log(result.affectedRows + " record(s) updated");
            res.redirect("/hrbranchaddit?id="+data.branch_id)
        });
      });
}
controller.fixcosteditform = (req,res) =>{
    id = req.query.id;
    branch_id = req.query.branch_id;
    req.getConnection((err, conn) => {
        conn.query(`SELECT * FROM fixcost Where fixcost_id = '${id}'`, (err, fixcost) => {
            if (err) {
                res.json(err);
            }
            let data = {fixcost_id:fixcost[0].fixcost_id,fixcost_title:fixcost[0].fixcost_title,fixcost_price:fixcost[0].fixcost_price
                ,fixcost_note:fixcost[0].fixcost_note }
            console.log(fixcost)
            res.render("0FixcostEdit", { data: data , branch_id:branch_id});
        });
    });
}
controller.fixcosteditsave = (req,res) =>{
    req.getConnection((err, conn) => {
        const data = {
            id:req.body.fixcost_id,
            addDate: Date.now(),
            updateDate: Date.now(),
            fixcost_title: req.body.fixcost_title,
            fixcost_price: req.body.fixcost_price,
            fixcost_note: req.body.fixcost_note,
            branch_id: req.body.branch_id
        }
        console.log("update: " + JSON.stringify(data))
        sql = `UPDATE fixcost SET updatedAt = '${data.updateDate}', fixcost_title = '${data.fixcost_title}'
        , fixcost_price = '${data.fixcost_price}', fixcost_note = '${data.fixcost_note}'
        WHERE fixcost_id = '${data.id}'`;
        conn.query(sql, function (err, result) {
            if (err) throw err;
            console.log(result.affectedRows + " record(s) updated");
            res.redirect("/hrbranchaddit?id="+data.branch_id)
        });
      });
}
function parseDMY(s) {
    var b = s.split(/\D/);
    var d = new Date(b[2], --b[1], b[0]);
    return d && d.getMonth() == b[1] ? d : new Date(NaN);
  }
module.exports = controller;