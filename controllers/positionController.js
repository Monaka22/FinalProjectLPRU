const controller = {};
// const bodyPar = require('../app');
// controller.use(bodyPar());
const alert = require('alert-node');
controller.list = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM position Where status = 1', (err, positions) => {
            if (err) {
                res.json(err);
            }
            res.render("positionDatatable", { header: "ตำแหน่ง", data: positions });
        });
    });
};

controller.addform = (req,res) =>{
    res.render("positionAdd");
}

controller.save = (req, res) => {
    req.getConnection((err, conn) => {
        const data = {
            addDate: Date.now(),
            updateDate: Date.now(),
            position_name: req.body.position_name,
            status: 1
        }
        conn.query(`SELECT * FROM position Where position_name = '${data.position_name}' AND status = 1`, (err, positions) => {
            if(positions.length == 0){
                console.log(data);
                const sql = `INSERT INTO position  VALUES ('${data.addDate}','${data.updateDate}','${null}','${data.position_name}','${data.status}')`;
                conn.query(sql, function (err, result) {
                    if (err) throw err;
                    console.log("1 record inserted");
                    res.redirect('/position')
                });
            }else{
                alert("ข้อมูลซ้ำ");
                res.render("positionAdd");
            }
        });
    });
};
controller.editform =(req,res)=>{
    id = req.query.id;
    req.getConnection((err, conn) => {
        conn.query(`SELECT * FROM position Where position_id = '${id}'`, (err, positions) => {
            if (err) {
                res.json(err);
            }
            console.log(positions)
            res.render("positionEdit", { data: positions });
        });
    });
    
}

controller.update = (req, res) => {
    req.getConnection((err, conn) => {

        const data = {
            position_name: req.body.position_name,
            updateDate: Date.now(),
            id: req.body.position_id
        }
        console.log("update: " + JSON.stringify(data))
        sql = `UPDATE position SET updatedAt = '${data.updateDate}', position_name = '${data.position_name}' WHERE position_id = '${data.id}'`;
        conn.query(sql, function (err, result) {
            if (err) throw err;
            console.log(result.affectedRows + " record(s) updated");
            res.redirect('/position')
        });
    });
};

controller.delete = (req, res) => {
    req.getConnection((err, conn) => {
       let id = req.query.id;
       let status = 0
        console.log(id);
        sql = `UPDATE position SET status = '${status}' WHERE position_id = '${id}'`;
        conn.query(sql, function (err, result) {
            if (err) throw err;
            console.log("1 delete inserted");
            res.redirect('/position')
        });
    });
}

module.exports = controller;