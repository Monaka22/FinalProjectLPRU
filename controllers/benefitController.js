const controller = {};
// const bodyPar = require('../app');
// controller.use(bodyPar());
const moment = require('moment')
controller.list = (req, res) => {
    let id = req.query.id;
    req.getConnection((err, conn) => {
        conn.query(`SELECT * FROM benefit Where benefit_emp_id = '${id}'`, (err, benefit) => {
            if (err) {
                res.json(err);
            }
            req.getConnection((err, conn) => {
                conn.query(`SELECT * FROM employees Where emp_id = '${id}'`, (err, emp) => {
                    if (err) {
                        res.json(err);
                    }
                    res.render("benefitDatatable", {
                        header: "สิทธิประโยชน์",
                        data: benefit,
                        emp_name: emp[0].emp_name,
                        emp_id:emp[0].emp_id
                    });
                });
            });
        });
    });
};

controller.addform = (req, res) => {
        let id = req.query.id;
        res.render("benefitAdd",{emp_id: id} )
}

controller.save = (req, res) => {
    req.getConnection((err, conn) => {
        let date = req.body.benefit_date;
        date = date.split("-");
        let newdate = date[0] + "/" + date[1] + "/" + date[2];
        let numdate = parseDMY(newdate).getTime();
        const data = {
            addDate: Date.now(),
            updateDate: Date.now(),
            benefit_title: req.body.benefit_title,
            benefit_price: req.body.benefit_price,
            benefit_date: numdate,
            benefit_note: req.body.benefit_note,
            benefit_emp_id: req.body.emp_id
        }
        console.log(data);
        const sql = `INSERT INTO benefit  VALUES ('${data.addDate}','${data.updateDate}','${null}','${data.benefit_title}','${data.benefit_price}'
        ,'${data.benefit_date}','${data.benefit_note}','${data.benefit_emp_id}')`;
        conn.query(sql, function (err, result) {
            if (err) throw err;
            console.log("1 record inserted");
            res.redirect("/benefit?id="+data.benefit_emp_id)
        });
    });
};
controller.editform = (req, res) => {
    id = req.query.id;
    emp_id = req.query.emp_id;
    req.getConnection((err, conn) => {
        conn.query(`SELECT * FROM benefit Where benefit_id = '${id}'`, (err, benefits) => {
            if (err) {
                res.json(err);
            }
            let data = {benefit_id:benefits[0].benefit_id,benefit_title:benefits[0].benefit_title,benefit_price:benefits[0].benefit_price
                ,benefit_date:moment(benefits[0].benefit_date).format("DD/MM/YYYY")
                ,benefit_note:benefits[0].benefit_note}
            console.log(benefits)
            res.render("benefitEdit", { data: data , emp_id:emp_id});
        });
    });

}

controller.update = (req, res) => {
    req.getConnection((err, conn) => {
        let date = req.body.benefit_date;
        console.log(date)
       date = date.split("-");
        let newdate = date[0] + "/" + date[1] + "/" + date[2];
        let numdate = parseDMY(newdate).getTime();
        const data = {
            id:req.body.benefit_id,
            addDate: Date.now(),
            updateDate: Date.now(),
            benefit_title: req.body.benefit_title,
            benefit_price: req.body.benefit_price,
            benefit_date: numdate,
            benefit_note: req.body.benefit_note,
            benefit_emp_id: req.body.emp_id
        }
        console.log("update: " + JSON.stringify(data))
        sql = `UPDATE benefit SET updatedAt = '${data.updateDate}', benefit_title = '${data.benefit_title}'
        , benefit_price = '${data.benefit_price}', benefit_date = '${data.benefit_date}'
        , benefit_note = '${data.benefit_note}' WHERE benefit_id = '${data.id}'`;
        conn.query(sql, function (err, result) {
            if (err) throw err;
            console.log(result.affectedRows + " record(s) updated");
            res.redirect("/benefit?id="+data.benefit_emp_id)
        });
      });
};

controller.delete = (req, res) => {
    req.getConnection((err, conn) => {
        id = req.query.id;
        emp_id = req.query.emp_id;
        console.log(id);
        sql = `DELETE FROM benefit WHERE benefit_id='${id}'`;
        conn.query(sql, function (err, result) {
            if (err) throw err;
            console.log("1 delete inserted");
            res.redirect("/benefit?id="+emp_id)
        });
    });
}
function parseDMY(s) {
    var b = s.split(/\D/);
    var d = new Date(b[2], --b[1], b[0]);
    return d && d.getMonth() == b[1] ? d : new Date(NaN);
  }
module.exports = controller;