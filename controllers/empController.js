const controller = {};
// const bodyPar = require('../app');
// controller.use(bodyPar());
const alert = require('alert-node');
controller.list = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM employees Where status = 1', (err, emps) => {
            if (err) {
                res.json(err);
            }
            res.render("empDatatable", { header: "พนักงาน", data: emps });
        });
    });
};

controller.addform = (req,res) =>{
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM branch Where status = 1', (err, branch) => {
            if (err) {
                res.json(err);
            }
            res.render("empAdd", { data: branch });
        });
    });
}

controller.save = (req, res) => {
    req.getConnection((err, conn) => {
        const data = {
            addDate: Date.now(),
            updateDate: Date.now(),
            emp_name: req.body.emp_name,
            emp_nickname: req.body.emp_nickname,
            emp_salary:req.body.emp_salary,
            emp_address : req.body.emp_address,
            emp_tel: req.body.emp_tel,
            emp_emer_con_name:req.body.emp_emer_con_name,
            emp_emer_con_relation:req.body.emp_emer_con_relation,
            emp_emer_con_address:req.body.emp_emer_con_address,
            emp_emer_con_tel:req.body.emp_emer_con_tel,
            emp_branch_id:req.body.branch_id,
            status: 1
        }
        conn.query(`SELECT * FROM employees Where emp_name = '${data.emp_name}' AND status = 1`, (err, emps) => {
            if(emps.length == 0){
                console.log(data);
                const sql = `INSERT INTO employees  VALUES ('${data.addDate}','${data.updateDate}','${null}','${data.emp_name}','${data.emp_nickname}'
                ,'${data.emp_salary}','${data.emp_address}','${null}','${data.emp_tel}','${data.emp_emer_con_name}'
                ,'${data.emp_emer_con_relation}','${data.emp_emer_con_address}','${data.emp_emer_con_tel}'
                ,'${data.status}','${data.emp_branch_id}')`;
                conn.query(sql, function (err, result) {
                     if (err) throw err;
                    console.log("1 record inserted");
                    res.redirect('/emp')
               });
            }else{
                alert("ข้อมูลซ้ำ");
                res.redirect('/empadd')
            }
        });
        
    });
};
controller.editform =(req,res)=>{
    id = req.query.id;
    req.getConnection((err, conn) => {
        conn.query(`SELECT
        employees.emp_id,
        employees.emp_name,
        employees.emp_nickname,
        employees.emp_salary,
        employees.emp_address,
        employees.emp_tel,
        employees.emp_emer_con_name,
        employees.emp_emer_con_relation,
        employees.emp_emer_con_address,
        employees.emp_emer_con_tel,
        employees.emp_branch_id,
        branch.branch_id,
        branch.branch_name
        FROM
        employees
        INNER JOIN branch ON employees.emp_branch_id = branch.branch_id Where emp_id = '${id}'`, (err, emps) => {
            if (err) {
                res.json(err);
            }
            req.getConnection((err, conn) => {
                conn.query('SELECT * FROM branch Where status = 1', (err, branch) => {
                    if (err) {
                        res.json(err);
                    }
                    res.render("empEdit", { data: emps ,data2:branch });
                });
            });
        });
    });
    
}

controller.update = (req, res) => {
    req.getConnection((err, conn) => {

        const data = {
            id:req.body.emp_id,
            updateDate: Date.now(),
            emp_name: req.body.emp_name,
            emp_nickname: req.body.emp_nickname,
            emp_salary:req.body.emp_salary,
            emp_address : req.body.emp_address,
            emp_tel: req.body.emp_tel,
            emp_emer_con_name:req.body.emp_emer_con_name,
            emp_emer_con_relation:req.body.emp_emer_con_relation,
            emp_emer_con_address:req.body.emp_emer_con_address,
            emp_emer_con_tel:req.body.emp_emer_con_tel,
            emp_branch_id:req.body.branch_id,
        }
        console.log("update: " + JSON.stringify(data))
        sql = `UPDATE employees SET updatedAt = '${data.updateDate}', emp_name = '${data.emp_name}'
        , emp_nickname = '${data.emp_nickname}', emp_salary = '${data.emp_salary}'
        , emp_address = '${data.emp_address}', emp_tel = '${data.emp_tel}'
        , emp_emer_con_name = '${data.emp_emer_con_name}'
        , emp_emer_con_relation = '${data.emp_emer_con_relation}'
        , emp_emer_con_address = '${data.emp_emer_con_address}'
        , emp_emer_con_tel = '${data.emp_emer_con_tel}'
        , emp_branch_id = '${data.emp_branch_id}' WHERE emp_id = '${data.id}'`;
        conn.query(sql, function (err, result) {
            if (err) throw err;
            console.log(result.affectedRows + " record(s) updated");
            res.redirect('/emp')
        });
    });
};

controller.delete = (req, res) => {
    req.getConnection((err, conn) => {
       let id = req.query.id;
       let status = 0
        console.log(id);
        sql = `UPDATE employees SET status = '${status}' WHERE emp_id = '${id}'`;
        conn.query(sql, function (err, result) {
            if (err) throw err;
            console.log("1 delete inserted");
            res.redirect('/emp')
        });
    });
}

module.exports = controller;