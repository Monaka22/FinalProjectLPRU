const controller = {};
// const bodyPar = require('../app');
// controller.use(bodyPar());

controller.list = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query(`SELECT
        positionemployee.position_emp_id,
        positionemployee.position_id,
        positionemployee.position_emp_id,
        position.position_name,
        position.position_id,
        employees.emp_id
        FROM
        position ,
        positionemployee,
        employees
        WHERE position.position_id=positionemployee.position_id AND positionemployee.emp_id = employees.emp_id AND position.status = 1 AND employees.status = 1`, (err, emppositions) => {
        if (err) {
                res.json(err);
            }
            let emppositionname = [];
            for (let i = 0; i < emppositions.length; i++) {
                emppositionname.push(emppositions[i].position_name)
            }
            // console.log(emppositionname)
            req.getConnection((err, conn) => {
                conn.query(`SELECT * FROM position WHERE status = 1`, (err, positions) => {
                    if (err) {
                        res.json(err);
                    }
                    let positionname = [];
                    for (let i = 0; i < positions.length; i++) {
                        positionname.push(positions[i].position_name)
                    }
                    //console.log(positionname)
                    total = 0;
                    positiontotal = [];
                    for (i = 0; i < positionname.length; i++) {
                        for (j = 0; j < emppositionname.length; j++) {
                            if (positionname[i] == emppositionname[j]) {
                                total++
                            }
                        }
                        positiontotal.push(total);
                        total = 0;
                    }
                    //console.log(positiontotal)
                    var jsonObj = {}
                    var array = []

                    for (i = 0; i < positionname.length; i++) {
                        array.push({
                            y: positiontotal[i],
                            name: positionname[i]
                        })
                        jsonObj = array;
                    }
                    //console.log(jsonObj)
                    res.render("0dashboard", {
                        positionEmp: jsonObj
                    });
                })
            })
        });
    });
};
controller.list2 = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query(`SELECT
        employees.emp_id,
        employees.emp_name,
        employees.emp_nickname,
        employees.emp_salary,
        benefit.benefit_price,
        benefit.benefit_emp_id
        FROM
        employees ,
        benefit
        WHERE employees.emp_id=benefit.benefit_emp_id AND employees.status = 1 ORDER BY emp_id`, (err, bnfemp) => {
            conn.query('SELECT * FROM employees WHERE status = 1', (err, emp) => {
                let emp_id = [];
                let emp_name = [];
                let emp_nickname = [];
                let mandayArray = [];
                for (let i = 0; i < emp.length; i++) {
                    let benefit = 0
                    let emp_salary = 0
                    for (let j = 0; j < bnfemp.length; j++) {
                        if (emp[i].emp_id == bnfemp[j].emp_id) {
                            benefit += (+bnfemp[j].benefit_price);
                        }
                    }
                    emp_salary = emp[i].emp_salary
                    emp_id.push(emp[i].emp_id)
                    emp_name.push(emp[i].emp_name)
                    emp_nickname.push(emp[i].emp_nickname)
                    benefit = benefit + (+emp_salary)
                    let manday = benefit / 20
                    mandayArray.push(manday)
                }
                var jsonObj = {}
                var array = []

                for (i = 0; i < emp_id.length; i++) {
                    array.push({
                        emp_id: emp_id[i],
                        emp_name: emp_name[i],
                        emp_nickname: emp_nickname[i],
                        manday: formatNumber(mandayArray[i])
                    })
                    jsonObj = array;
                }
                res.render("0dashboard2", {
                    data: jsonObj
                });
            });

        });
    });

}
controller.list3 = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query(`SELECT * FROM projectmanage WHERE status = 1`, (err, projectmanage) => {
            let total = 0
            let total2 = 0
            for (let i = 0; i < projectmanage.length; i++) {
                total += projectmanage[i].project_total_cost
                total2 += projectmanage[i].project_total_selling,
                projectmanage[i].project_total_cost = formatNumber(projectmanage[i].project_total_cost),
                projectmanage[i].project_total_selling = formatNumber(projectmanage[i].project_total_selling)
            }
            res.render("0dashboard3", {
                data: projectmanage,
                totalcost: formatNumber(total),
                totalselling: formatNumber(total2)
            });
        });
    })
};
controller.list4 = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query(`SELECT * FROM projectmanage WHERE project_start_date >= 1546300800000 AND project_end_date <=1577836799000 AND status =1 `, (err, pro2019) => {
            let pro2019total = 0
            let pro2019total2 = 0
            for (let i = 0; i < pro2019.length; i++) {
                pro2019total += pro2019[i].project_total_cost
                pro2019total2 += pro2019[i].project_total_selling
            }
            conn.query(`SELECT * FROM projectmanage WHERE project_start_date >= 1514764800000 AND project_end_date <=1546300799000 AND status =1 `, (err, pro2018) => {
                let pro2018total = 0
                let pro2018total2 = 0
                for (let i = 0; i < pro2018.length; i++) {
                    pro2018total += pro2018[i].project_total_cost
                    pro2018total2 += pro2018[i].project_total_selling
                }
                conn.query(`SELECT * FROM projectmanage WHERE project_start_date >= 1483228800000 AND project_end_date <=1514764799000 AND status =1 `, (err, pro2017) => {
                    let pro2017total = 0
                    let pro2017total2 = 0
                    for (let i = 0; i < pro2017.length; i++) {
                        pro2017total += pro2017[i].project_total_cost
                        pro2017total2 += pro2017[i].project_total_selling
                    }
                    conn.query(`SELECT * FROM projectmanage WHERE project_start_date >= 1451606400000 AND project_end_date <=1483228799000 AND status =1 `, (err, pro2016) => {
                        let pro2016total = 0
                        let pro2016total2 = 0
                        for (let i = 0; i < pro2016.length; i++) {
                            pro2016total += pro2016[i].project_total_cost
                            pro2016total2 += pro2016[i].project_total_selling
                        }
                        conn.query(`SELECT * FROM projectmanage WHERE project_start_date >= 1420070400000 AND project_end_date <=1451606399000 AND status =1 `, (err, pro2015) => {
                            let pro2015total = 0
                            let pro2015total2 = 0
                            for (let i = 0; i < pro2015.length; i++) {
                                pro2015total += pro2015[i].project_total_cost
                                pro2015total2 += pro2015[i].project_total_selling
                            }
                            res.render("0dashboard4", {
                                pro2019cost: (pro2019total),
                                pro2019sell: (pro2019total2),
                                pro2018cost: (pro2018total),
                                pro2018sell: (pro2018total2),
                                pro2017cost: (pro2017total),
                                pro2017sell: (pro2017total2),
                                pro2016cost: (pro2016total),
                                pro2016sell: (pro2016total2),
                                pro2015cost: (pro2015total),
                                pro2015sell: (pro2015total2)
                            });
                        });
                    });
                });
            });
        });
    })
};
controller.list41 = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query(`SELECT * FROM team  WHERE emp_start_date >= 1546300800000 AND emp_end_date <=1577836799000 ORDER BY emp_id DESC`, (err, team) => {
            conn.query('SELECT * FROM employees WHERE status = 1 ORDER BY emp_id DESC', (err, emp) => {
                let emp_id = [];
                let emp_name = [];
                let workdayArray = [];
                for (let i = 0; i < emp.length; i++) {
                    let workday = 0
                    for (let j = 0; j < team.length; j++) {
                        if (emp[i].emp_id == team[j].emp_id) {
                            workday += (+team[j].emp_workday);
                        }
                    }
                    emp_id.push(emp[i].emp_id)
                    emp_name.push(emp[i].emp_name)
                    workdayArray.push(workday)
                }
                var jsonObj2019 = {}
                var array2019 = []

                for (i = 0; i < emp_id.length; i++) {
                    array2019.push({
                        y: workdayArray[i],
                        label: emp_name[i],
                    })
                    jsonObj2019 = array2019;
                }
                conn.query(`SELECT * FROM team  WHERE emp_start_date >= 1514764800000 AND emp_end_date <=1546300799000 ORDER BY emp_id DESC`, (err, team) => {
                    conn.query('SELECT * FROM employees WHERE status = 1 ORDER BY emp_id DESC', (err, emp) => {
                        let emp_id = [];
                        let emp_name = [];
                        let workdayArray = [];
                        for (let i = 0; i < emp.length; i++) {
                            let workday = 0
                            for (let j = 0; j < team.length; j++) {
                                if (emp[i].emp_id == team[j].emp_id) {
                                    workday += (+team[j].emp_workday);
                                }
                            }
                            emp_id.push(emp[i].emp_id)
                            emp_name.push(emp[i].emp_name)
                            workdayArray.push(workday)
                        }
                        var jsonObj2018 = {}
                        var array2018 = []

                        for (i = 0; i < emp_id.length; i++) {
                            array2018.push({
                                y: workdayArray[i],
                                label: emp_name[i],
                            })
                            jsonObj2018 = array2018;
                        }
                        conn.query(`SELECT * FROM team  WHERE emp_start_date >= 1483228800000 AND emp_end_date <=1514764799000 ORDER BY emp_id DESC`, (err, team) => {
                            conn.query('SELECT * FROM employees WHERE status = 1 ORDER BY emp_id DESC', (err, emp) => {
                                let emp_id = [];
                                let emp_name = [];
                                let workdayArray = [];
                                for (let i = 0; i < emp.length; i++) {
                                    let workday = 0
                                    for (let j = 0; j < team.length; j++) {
                                        if (emp[i].emp_id == team[j].emp_id) {
                                            workday += (+team[j].emp_workday);
                                        }
                                    }
                                    emp_id.push(emp[i].emp_id)
                                    emp_name.push(emp[i].emp_name)
                                    workdayArray.push(workday)
                                }
                                var jsonObj2017 = {}
                                var array2017 = []

                                for (i = 0; i < emp_id.length; i++) {
                                    array2017.push({
                                        y: workdayArray[i],
                                        label: emp_name[i],
                                    })
                                    jsonObj2017 = array2017;
                                }
                                conn.query(`SELECT * FROM team  WHERE emp_start_date >= 1451606400000 AND emp_end_date <=1483228799000 ORDER BY emp_id DESC`, (err, team) => {
                                    conn.query('SELECT * FROM employees WHERE status = 1 ORDER BY emp_id DESC', (err, emp) => {
                                        let emp_id = [];
                                        let emp_name = [];
                                        let workdayArray = [];
                                        for (let i = 0; i < emp.length; i++) {
                                            let workday = 0
                                            for (let j = 0; j < team.length; j++) {
                                                if (emp[i].emp_id == team[j].emp_id) {
                                                    workday += (+team[j].emp_workday);
                                                }
                                            }
                                            emp_id.push(emp[i].emp_id)
                                            emp_name.push(emp[i].emp_name)
                                            workdayArray.push(workday)
                                        }
                                        var jsonObj2016 = {}
                                        var array2016 = []

                                        for (i = 0; i < emp_id.length; i++) {
                                            array2016.push({
                                                y: workdayArray[i],
                                                label: emp_name[i],
                                            })
                                            jsonObj2016 = array2016;
                                        }
                                        conn.query(`SELECT * FROM team  WHERE emp_start_date >= 1420070400000 AND emp_end_date <=1451606399000 ORDER BY emp_id DESC`, (err, team) => {
                                            conn.query('SELECT * FROM employees WHERE status = 1 ORDER BY emp_id DESC', (err, emp) => {
                                                let emp_id = [];
                                                let emp_name = [];
                                                let workdayArray = [];
                                                for (let i = 0; i < emp.length; i++) {
                                                    let workday = 0
                                                    for (let j = 0; j < team.length; j++) {
                                                        if (emp[i].emp_id == team[j].emp_id) {
                                                            workday += (+team[j].emp_workday);
                                                        }
                                                    }
                                                    emp_id.push(emp[i].emp_id)
                                                    emp_name.push(emp[i].emp_name)
                                                    workdayArray.push(workday)
                                                }
                                                var jsonObj2015 = {}
                                                var array2015 = []

                                                for (i = 0; i < emp_id.length; i++) {
                                                    array2015.push({
                                                        y: workdayArray[i],
                                                        label: emp_name[i],
                                                    })
                                                    jsonObj2015 = array2015;
                                                }
                                                console.log(jsonObj2019)
                                                res.render("0dashboard41", {
                                                    data2015: jsonObj2015,
                                                    data2016: jsonObj2016,
                                                    data2017: jsonObj2017,
                                                    data2018: jsonObj2018,
                                                    data2019: jsonObj2019,
                                                });
                                            });

                                        });
                                    });
                                });
                            });

                        });
                    });
                });
            });

        });
    });
};
function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }
module.exports = controller;