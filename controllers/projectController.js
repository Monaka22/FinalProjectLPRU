const controller = {};
const moment = require('moment');
// const bodyPar = require('../app');
// controller.use(bodyPar());
const alert = require('alert-node');
controller.list = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM projectmanage Where status = 1', (err, projects) => {
            if (err) {
                res.json(err);
            }
            res.render("projectDatatable", {
                header: "โครงการ",
                data: projects
            });
        });
    });
};

controller.addform = (req, res) => {
    res.render("projectAdd");
}

controller.save = (req, res) => {
    req.getConnection((err, conn) => {
        let start = req.body.project_start_date;
        let end = req.body.project_end_date;
        start = start.split("-");
        end = end.split("-");
        let newStart = start[0] + "/" + start[1] + "/" + start[2];
        let newEnd = end[0] + "/" + end[1] + "/" + end[2];
        let numStart = parseDMY(newStart).getTime();
        let numEnd = parseDMY(newEnd).getTime();
        const data = {
            addDate: Date.now(),
            updateDate: Date.now(),
            project_name: req.body.project_name,
            project_costomer_name: req.body.project_costomer_name,
            project_start_date: numStart,
            project_end_date: numEnd,
            project_team_name: req.body.project_team_name,
            selling: req.body.selling,
            project_note: req.body.project_note,
            status: 1,
        }
        conn.query(`SELECT * FROM projectmanage Where project_name = '${data.project_name}' AND status = 1`, (err, projects) => {
            if(projects.length == 0){
                console.log(data);
                const sql = `INSERT INTO projectmanage (createdAt, updatedAt, project_name, project_costomer_name, project_start_date, project_end_date,project_team_name,selling,project_note,status,project_total_cost,project_total_selling) 
                VALUES ('${data.addDate}','${data.updateDate}','${data.project_name}','${data.project_costomer_name}','${data.project_start_date}','${data.project_end_date}'
                ,'${data.project_team_name}','${data.selling}','${data.project_note}','${data.status}',0,0)`;
                conn.query(sql, function (err, result) {
                    if (err) throw err;
                    console.log("1 record inserted");
                    res.redirect('/project')
                });
            }else{
                alert("ข้อมูลซ้ำ");
                res.render("projectAdd");
            }
        });
        
    });
};
controller.projectform = (req, res) => {
    id = req.query.id;
    req.getConnection((err, conn) => {
        conn.query(`SELECT * FROM projectmanage Where project_id = '${id}'`, (err, projects) => {
            if (err) {
                res.json(err);
            }
            let data = {
                project_id: projects[0].project_id,
                project_name: projects[0].project_name,
                project_costomer_name: projects[0].project_costomer_name,
                project_start_date: moment(projects[0].project_start_date).format("DD/MM/YYYY"),
                project_end_date: moment(projects[0].project_end_date).format("DD/MM/YYYY"),
                project_team_name: projects[0].project_team_name,
                selling: projects[0].selling,
                project_note: projects[0].project_note,
                project_total_cost: formatNumber(projects[0].project_total_cost),
                project_total_selling: formatNumber(projects[0].project_total_selling)
            }
            console.log(data)
            res.render("project1", {
                item: data
            });
        });
    });
}
controller.editform = (req, res) => {
    id = req.query.id;
    req.getConnection((err, conn) => {
        conn.query(`SELECT * FROM projectmanage Where project_id = '${id}'`, (err, projects) => {
            if (err) {
                res.json(err);
            }
            let data = {
                project_id: projects[0].project_id,
                project_name: projects[0].project_name,
                project_costomer_name: projects[0].project_costomer_name,
                project_start_date: moment(projects[0].project_start_date).format("DD/MM/YYYY"),
                project_end_date: moment(projects[0].project_end_date).format("DD/MM/YYYY"),
                project_team_name: projects[0].project_team_name,
                selling: projects[0].selling,
                project_note: projects[0].project_note
            }
            console.log(data)
            res.render("projectEdit", {
                item: data
            });
        });
    });
}
controller.update = (req, res) => {
    req.getConnection((err, conn) => {
        let id = req.body.project_id;
        let start = req.body.project_start_date;
        let end = req.body.project_end_date;
        start = start.split("-");
        end = end.split("-");
        let newStart = start[0] + "/" + start[1] + "/" + start[2];
        let newEnd = end[0] + "/" + end[1] + "/" + end[2];
        let numStart = parseDMY(newStart).getTime();
        let numEnd = parseDMY(newEnd).getTime();
        const data = {
            id: id,
            updateDate: Date.now(),
            project_name: req.body.project_name,
            project_costomer_name: req.body.project_costomer_name,
            project_start_date: numStart,
            project_end_date: numEnd,
            project_team_name: req.body.project_team_name,
            selling: req.body.selling,
            project_note: req.body.project_note,
        }
        console.log("update: " + JSON.stringify(data))
                conn.query(`SELECT * FROM projectmanage Where project_id = '${data.id}'`, (err, projects) => {
                    let project_cost = projects[0].project_total_cost;
                    let project_selling = ((+project_cost) * ((+data.selling) / 100)) + (+project_cost);
                    sql = `UPDATE projectmanage SET updatedAt = '${data.updateDate}', project_name = '${data.project_name}', project_costomer_name = '${data.project_costomer_name}'
                    , project_start_date = '${data.project_start_date}', project_end_date = '${data.project_end_date}', project_team_name = '${data.project_team_name}'
                    , selling = '${data.selling}', project_note = '${data.project_note}',project_total_selling = '${project_selling}' WHERE project_id = '${data.id}'`;
                    conn.query(sql, function (err, result) {
                        if (err) throw err;
                        console.log(result.affectedRows + " record(s) updated");
                        res.redirect("/projectfrom?id=" + data.id)
                    });
                });
    });
};

controller.delete = (req, res) => {
    req.getConnection((err, conn) => {
        let id = req.query.id;
        let status = 0
        console.log(id);
        sql = `UPDATE projectmanage SET status = '${status}' WHERE project_id = '${id}'`;
        conn.query(sql, function (err, result) {
            if (err) throw err;
            console.log("1 delete inserted");
            res.redirect('/project')
        });
    });
}
controller.projectaddit = (req, res) => {
    let id = req.query.id;
    req.getConnection((err, conn) => {
        conn.query(`SELECT * FROM projectaddit Where project_id = '${id}'`, (err, projectaddit) => {
            if (err) {
                res.json(err);
            }
            for (let i = 0; i < projectaddit.length; i++) {
                projectaddit[i].project_addit_date = moment(projectaddit[i].project_addit_date).format("DD/MM/YYYY")
            }
            req.getConnection((err, conn) => {
                conn.query(`SELECT * FROM projectmanage Where project_id = '${id}'`, (err, projectmanage) => {
                    if (err) {
                        res.json(err);
                    }
                    res.render("project2", {
                        data: projectaddit,
                        project_name: projectmanage[0].project_name,
                        project_id: projectmanage[0].project_id,
                    });
                });
            });
        });
    });
};
controller.projectadditaddform = (req, res) => {
    let id = req.query.id;
    res.render("projectadditAdd", {
        project_id: id
    })
};
controller.projectadditsave = (req, res) => {
    req.getConnection((err, conn) => {
        let date = req.body.project_addit_date;
        date = date.split("-");
        let newdate = date[0] + "/" + date[1] + "/" + date[2];
        let numdate = parseDMY(newdate).getTime();
        const data = {
            addDate: Date.now(),
            updateDate: Date.now(),
            project_addit_title: req.body.project_addit_title,
            project_addit_price: req.body.project_addit_price,
            project_addit_date: numdate,
            project_id: req.body.project_id
        }
        console.log(data);
        const sql = `INSERT INTO projectaddit  VALUES ('${data.addDate}','${data.updateDate}','${null}','${data.project_addit_title}','${data.project_addit_date}'
        ,'${data.project_addit_price}','${data.project_id}')`;
        conn.query(sql, function (err, result) {
            if (err) throw err;
            req.getConnection((err, conn) => {
                req.getConnection((err, conn) => {
                    conn.query(`SELECT * FROM projectmanage Where project_id = '${data.project_id}'`, (err, projects) => {
                        if (err) {
                            res.json(err);
                        }
                        projects[0].project_total_cost = (+data.project_addit_price) + (+projects[0].project_total_cost)
                        projects[0].project_total_selling = ((+projects[0].project_total_cost) * ((+projects[0].selling) / 100)) + (+projects[0].project_total_cost)
                        conn.query(`UPDATE projectmanage SET project_total_cost = '${projects[0].project_total_cost}' ,
                        project_total_selling = '${projects[0].project_total_selling}'
                        WHERE project_id = '${data.project_id}'`, (err, result) => {
                            if (err) throw err;
                            res.redirect("/projectaddit?id=" + data.project_id)
                        });
                    });
                });
            });
            // console.log("1 record inserted");
            // res.redirect("/projectaddit?id="+data.project_id)
        });
    });
};
controller.projectadditdelete = (req, res) => {
    req.getConnection((err, conn) => {
        id = req.query.id;
        project_id = req.query.project_id;
        console.log(id);
        req.getConnection((err, conn) => {
            conn.query(`SELECT * FROM projectmanage Where project_id = '${project_id}'`, (err, projects) => {
                req.getConnection((err, conn) => {
                    conn.query(`SELECT * FROM projectaddit Where project_addit_id='${id}'`, (err, addit) => {
                        let project_cost = (+projects[0].project_total_cost) - (+addit[0].project_addit_price);
                        let project_selling = ((+project_cost) * ((+projects[0].selling) / 100)) + (+project_cost);
                        sql = `UPDATE projectmanage SET project_total_cost = '${project_cost}',project_total_selling = '${project_selling}' WHERE project_id = '${project_id}'`;
                        conn.query(sql, function (err, result) {
                            if (err) throw err;
                            sql = `DELETE FROM projectaddit WHERE project_addit_id='${id}'`;
                            conn.query(sql, function (err, result) {
                                if (err) throw err;
                                console.log("1 delete inserted");
                                res.redirect("/projectaddit?id=" + project_id)
                            });
                        });

                    })
                });
            })
        });
    });
}
controller.projectteam = (req, res) => {
    let id = req.query.id;
    req.getConnection((err, conn) => {
        conn.query(`SELECT
        projectmanage.project_id,
        team.team_id,
        team.emp_start_date,
        team.emp_end_date,
        employees.emp_id,
        employees.emp_name,
        position.position_id,
        position.position_name
        FROM
        projectmanage ,
        team ,
        employees ,
        position
        WHERE projectmanage.project_id = team.project_id AND team.emp_id = employees.emp_id AND team.position_id = position.position_id AND projectmanage.status = 1
        AND employees.status = 1 AND position.status = 1 AND projectmanage.project_id = '${id}'`, (err, team) => {
            if (err) {
                res.json(err);
            }
            console.log(team)
            for (let i = 0; i < team.length; i++) {
                team[i].emp_start_date = moment(team[i].emp_start_date).format("DD/MM/YYYY")
                team[i].emp_end_date = moment(team[i].emp_end_date).format("DD/MM/YYYY")
            }
            req.getConnection((err, conn) => {
                conn.query(`SELECT * FROM projectmanage Where project_id = '${id}'`, (err, projectmanage) => {
                    if (err) {
                        res.json(err);
                    }
                    res.render("project3", {
                        data: team,
                        project_name: projectmanage[0].project_name,
                        project_id: projectmanage[0].project_id,
                    });
                });
            });
        });
    });
}

controller.teamadd = (req, res) => {
    req.getConnection((err, conn) => {
        let id = req.query.id;
        conn.query('SELECT * FROM position Where status = 1', (err, position) => {
            if (err) {
                res.json(err);
            }
            conn.query(`SELECT * FROM projectmanage Where project_id = '${id}'`, (err, projectmanage) => {
                res.render("TeamAdd", {
                    data: position,
                    project_id: id,
                    project_start_date: moment(projectmanage[0].project_start_date).format("DD/MM/YYYY"),
                    project_end_date: moment(projectmanage[0].project_end_date).format("DD/MM/YYYY")
                });
            });
        });
    });
}
controller.teamfreetime = (req, res) => {
    req.getConnection((err, conn) => {
        let project_id = req.body.project_id;
        let position_id = req.body.position_id;
        let start = req.body.emp_start_date;
        let end = req.body.emp_end_date;
        start = start.split("-");
        end = end.split("-");
        let newStart = start[0] + "/" + start[1] + "/" + start[2];
        let newEnd = end[0] + "/" + end[1] + "/" + end[2];
        let emp_start = parseDMY(newStart).getTime();
        let emp_end = parseDMY(newEnd).getTime();
        conn.query(`SELECT * FROM projectmanage Where project_id = '${project_id}' status = 1`, (err, project) => {
            conn.query(`SELECT
            positionemployee.position_emp_id,
            positionemployee.emp_id,
            positionemployee.position_id,
            position.position_name,
            employees.emp_name,
            employees.emp_nickname
            FROM
            positionemployee ,
            position ,
            employees
            WHERE positionemployee.emp_id = employees.emp_id AND positionemployee.position_id = position.position_id AND position.position_id = '${position_id}' AND position.status = 1 AND employees.status=1`, (err, position) => {
                conn.query(`SELECT
                team.emp_start_date,
                team.emp_end_date,
                team.emp_id,
                team.project_id,
                team.position_id,
                position.position_name,
                employees.emp_name,
                employees.emp_nickname
                FROM
                team ,
                position ,
                employees
                WHERE team.position_id = position.position_id AND team.emp_id = employees.emp_id AND position.status = 1 AND employees.status = 1`, (err, team) => {
                    let emp_id_t = [];
                    let emp_name_t = [];
                    let emp_nickname_t = [];
                    let emp_position_id_t = [];
                    let emp_position_id = [];
                    let emp_end_date = [];
                    let emp_end_date_t = [];
                    let emp_id = [];
                    let emp_name = [];
                    let emp_nickname = [];
                    let emp_start_date = [];
                    let freetime = [];
                    let emp_start_date_t = [];
                    for (let i = 0; i < team.length; i++) {
                        emp_id_t.push(team[i].emp_id);
                        emp_name_t.push(team[i].emp_name);
                        emp_nickname_t.push(team[i].emp_nickname);
                        emp_end_date_t.push(team[i].emp_end_date);
                        emp_position_id_t.push(team[i].position_id);
                        emp_start_date_t.push(team[i].emp_end_date);
                    }
                    for (let i = 0; i < position.length; i++) {
                        emp_id.push(position[i].emp_id);
                        emp_name.push(position[i].emp_name);
                        emp_nickname.push(position[i].emp_nickname);
                        emp_position_id.push(position[i].position_id);
                        emp_end_date.push(0);
                        emp_start_date.push(0);
                    }
                    emp_id = emp_id.concat(emp_id_t);
                    emp_name = emp_name.concat(emp_name_t);
                    emp_nickname = emp_nickname.concat(emp_nickname_t);
                    emp_position_id = emp_position_id.concat(emp_position_id_t);
                    emp_end_date = emp_end_date.concat(emp_end_date_t);
                    emp_start_date = emp_start_date.concat(emp_start_date_t);
                    for (let j = 0; j < emp_id.length; j++) {
                        let empenddate = emp_end_date[j];
                        let empstartdate = emp_start_date[j];
                        if (empstartdate >= emp_end && empenddate <= emp_end) {
                            free = "ไม่ว่าง"
                        } else {
                            if (empstartdate >= emp_start) {
                                free = "ไม่ว่าง"
                            } else {
                                free = "พร้อมทำงาน"
                            }
                        }
                        freetime[j] = free;
                    }
                    console.log(emp_name)
                    console.log(freetime)
                    console.log(emp_start_date)
                    console.log(emp_end_date)
                    console.log(freetime)
                    for (let y = 0; y < freetime.length; y++) {
                        for (let z = 0; z < freetime.length; z++) {
                            if (emp_name[y] == emp_name[z]) {
                                if (freetime[y] == "ไม่ว่าง" || freetime[z] == "ไม่ว่าง") {
                                    freetime[y] = "ไม่ว่าง";
                                    freetime[z] = "ไม่ว่าง";
                                }

                            }
                        }
                    }
                    var jsonObj = {}
                    var array = []
                    for (i = 0; i < emp_position_id.length; i++) {
                        if (emp_position_id[i] == position_id) {
                            //sails.log(emp_position_id[i])
                            array.push({
                                emp_id: emp_id[i],
                                emp_name: emp_name[i],
                                emp_nickname: emp_nickname[i],
                                freetime: freetime[i],
                                position_id: emp_position_id[i]
                            })
                            jsonObj = array;
                        }
                    }
                    emp_id2 = []
                    emp_name2 = []
                    emp_nickname2 = []
                    freetime3 = []

                    for (let i = 0; i < jsonObj.length; i++) {
                        emp_id2.push(jsonObj[i].emp_id)
                        emp_name2.push(jsonObj[i].emp_name)
                        emp_nickname2.push(jsonObj[i].emp_nickname)
                        freetime3.push(jsonObj[i].freetime)
                    }
                    let push2 = [];
                    for (let q = 0; q < freetime3.length; q++) {
                        push2[q] = emp_id2[q] + "-" + emp_name2[q] + "-" + emp_nickname2[q] + "-" + freetime3[q];
                    }
                    push2 = Array.from(new Set(push2))
                    emp_id3 = []
                    emp_name3 = []
                    emp_nickname3 = []
                    freetime4 = []
                    for (let w = 0; w < push2.length; w++) {
                        push2[w] = push2[w].split("-");
                        emp_id3[w] = Number(push2[w][0]);
                        emp_name3[w] = push2[w][1];
                        emp_nickname3[w] = push2[w][2];
                        freetime4[w] = push2[w][3];
                    }
                    var jsonObj2 = {}
                    var array2 = []
                    for (i = 0; i < freetime4.length; i++) {
                        //sails.log(emp_position_id[i])
                        array2.push({
                            project_id: project_id,
                            position_id: position_id,
                            position_name: position[0].position_name,
                            emp_id: emp_id3[i],
                            emp_name: emp_name3[i],
                            emp_nickname: emp_nickname3[i],
                            freetime: freetime4[i],
                            emp_start_date: emp_start,
                            emp_end_date: emp_end
                        })
                        jsonObj2 = array2;

                    }
                    data = jsonObj2;
                    res.render("FreetimeDatatable", {
                        data: data,
                    });


                });
            });
        });
    });
}
controller.teamsave = (req, res) => {
    req.getConnection((err, conn) => {
        let project_id = req.query.project_id;
        let emp_id = req.query.emp_id;
        let position_id = req.query.position_id;
        let emp_start_date = req.query.emp_start_date;
        let emp_end_date = req.query.emp_end_date;
        let day = req.query.emp_end_date - req.query.emp_start_date;
        console.log(day)
        day = Math.floor((day / (3600 * 24)) / 1000);
        console.log(day)
        let benefit = 0;
        let emp_salary = 0;
        conn.query(`SELECT
        employees.emp_id,
        employees.emp_name,
        employees.emp_nickname,
        employees.emp_salary,
        benefit.benefit_price,
        benefit.benefit_emp_id
        FROM
        employees ,
        benefit
        WHERE employees.emp_id=benefit.benefit_emp_id AND employees.status = 1 AND employees.emp_id = '${emp_id}'`, (err, bnfemp) => {
            for (let i = 0; i < bnfemp.length; i++) {
                benefit += (+bnfemp[i].benefit_price);
                emp_salary = bnfemp[i].emp_salary
            }
            console.log(benefit)
            benefit = benefit + (+emp_salary)
            console.log(benefit)
            let manday = benefit / 20
            console.log(manday)
            let cost = manday * day
            console.log(day)
            console.log(cost)
            conn.query(`SELECT * FROM projectmanage Where project_id = '${project_id}'`, (err, projects) => {
                projects[0].project_total_cost = (+cost) + (+projects[0].project_total_cost)
                projects[0].project_total_selling = ((+projects[0].project_total_cost) * ((+projects[0].selling) / 100)) + (+projects[0].project_total_cost)
                conn.query(`UPDATE projectmanage SET project_total_cost = '${projects[0].project_total_cost}' ,
                project_total_selling = '${projects[0].project_total_selling}'
                WHERE project_id = '${project_id}'`, (err, result) => {
                    conn.query(`INSERT INTO team  VALUES ('${null}','${null}','${null}','${emp_start_date}','${emp_end_date}'
                    ,'${day}','${null}','${emp_id}','${project_id}','${position_id}')`, (err, projects) => {
                        req.getConnection((err, conn) => {
                            conn.query(`SELECT
                            projectmanage.project_id,
                            team.emp_start_date,
                            team.emp_end_date,
                            employees.emp_id,
                            employees.emp_name,
                            position.position_id,
                            position.position_name
                            FROM
                            projectmanage ,
                            team ,
                            employees ,
                            position
                            WHERE projectmanage.project_id = team.project_id AND team.emp_id = employees.emp_id AND team.position_id = position.position_id AND projectmanage.status = 1
                            AND employees.status = 1 AND position.status = 1 AND projectmanage.project_id = '${project_id}'`, (err, team) => {
                                if (err) {
                                    res.json(err);
                                }
                                console.log(team)
                                for (let i = 0; i < team.length; i++) {
                                    team[i].emp_start_date = moment(team[i].emp_start_date).format("DD/MM/YYYY")
                                    team[i].emp_end_date = moment(team[i].emp_end_date).format("DD/MM/YYYY")
                                }
                                req.getConnection((err, conn) => {
                                    conn.query(`SELECT * FROM projectmanage Where project_id = '${project_id}'`, (err, projectmanage) => {
                                        if (err) {
                                            res.json(err);
                                        }
                                        res.redirect('/projectteam?id='+project_id);
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
}
controller.teamdelete = (req, res) => {
    req.getConnection((err, conn) => {
        let project_id = req.query.project_id;
        let emp_id = req.query.emp_id;
        let team_id = req.query.team_id;
        let benefit = 0;
        let emp_salary = 0;
        conn.query(`SELECT
        employees.emp_id,
        employees.emp_name,
        employees.emp_nickname,
        employees.emp_salary,
        benefit.benefit_price,
        benefit.benefit_emp_id
        FROM
        employees ,
        benefit
        WHERE employees.emp_id=benefit.benefit_emp_id AND employees.status = 1 AND employees.emp_id = '${emp_id}'`, (err, bnfemp) => {
            conn.query(`SELECT * FROM team WHERE team_id = '${team_id}' `, (err, team) => {
                let day = team[0].emp_workday;
                for (let i = 0; i < bnfemp.length; i++) {
                    benefit += (+bnfemp[i].benefit_price);
                    emp_salary = bnfemp[i].emp_salary
                }
                console.log(benefit)
                benefit = benefit + (+emp_salary)
                console.log(benefit)
                let manday = benefit / 20
                console.log(manday)
                let cost = manday * day
                console.log(day)
                console.log(cost)
                conn.query(`SELECT * FROM projectmanage Where project_id = '${project_id}'`, (err, projects) => {
                    projects[0].project_total_cost = (+projects[0].project_total_cost) - (+cost);
                    projects[0].project_total_selling = ((+projects[0].project_total_cost) * ((+projects[0].selling) / 100)) + (+projects[0].project_total_cost)
                    conn.query(`UPDATE projectmanage SET project_total_cost = '${projects[0].project_total_cost}' ,
                project_total_selling = '${projects[0].project_total_selling}'
                WHERE project_id = '${project_id}'`, (err, result) => {
                        conn.query(`DELETE FROM team WHERE team_id ='${team_id}'`, (err, projects) => {
                            req.getConnection((err, conn) => {
                                conn.query(`SELECT
                            projectmanage.project_id,
                            team.emp_start_date,
                            team.emp_end_date,
                            employees.emp_id,
                            employees.emp_name,
                            position.position_id,
                            position.position_name
                            FROM
                            projectmanage ,
                            team ,
                            employees ,
                            position
                            WHERE projectmanage.project_id = team.project_id AND team.emp_id = employees.emp_id AND team.position_id = position.position_id AND projectmanage.status = 1
                            AND employees.status = 1 AND position.status = 1 AND projectmanage.project_id = '${project_id}'`, (err, team) => {
                                    if (err) {
                                        res.json(err);
                                    }
                                    console.log(team)
                                    for (let i = 0; i < team.length; i++) {
                                        team[i].emp_start_date = moment(team[i].emp_start_date).format("DD/MM/YYYY")
                                        team[i].emp_end_date = moment(team[i].emp_end_date).format("DD/MM/YYYY")
                                    }
                                    req.getConnection((err, conn) => {
                                        conn.query(`SELECT * FROM projectmanage Where project_id = '${project_id}'`, (err, projectmanage) => {
                                            if (err) {
                                                res.json(err);
                                            }
                                            res.redirect('/projectteam?id='+project_id);
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });

    });
}

function parseDMY(s) {
    var b = s.split(/\D/);
    var d = new Date(b[2], --b[1], b[0]);
    return d && d.getMonth() == b[1] ? d : new Date(NaN);
}
function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

module.exports = controller;