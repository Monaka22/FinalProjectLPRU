const controller = {};
// const bodyPar = require('../app');
// controller.use(bodyPar());

controller.list = (req, res) => {
    let id = req.query.id;
    req.getConnection((err, conn) => {
        conn.query(`SELECT
        positionemployee.position_emp_id,
        positionemployee.emp_id,
        positionemployee.position_id,
        position.position_id,
        position.position_name,
        position.status
        FROM
        positionemployee ,
        position
        WHERE
        positionemployee.position_id = position.position_id AND position.status = 1 AND positionemployee.emp_id= '${id}'`, (err, positionemployee) => {
            if (err) {
                res.json(err);
            }
            req.getConnection((err, conn) => {
                conn.query(`SELECT * FROM employees Where emp_id = '${id}'`, (err, emp) => {
                    if (err) {
                        res.json(err);
                    }
                    res.render("0positionEmpDatatable", {
                        header: "ตำแหน่ง",
                        data: positionemployee,
                        emp_name: emp[0].emp_name,
                        emp_id:emp[0].emp_id
                    });
                });
            });
        });
    });
};

controller.addform = (req, res) => {
    req.getConnection((err, conn) => {
        let id = req.query.id;
        conn.query('SELECT * FROM position Where status = 1', (err, position) => {
            if (err) {
                res.json(err);
            }
            res.render("0positionEmpAdd", {
                data: position,
                emp_id:id
            });
        });
    });
}

controller.save = (req, res) => {
    req.getConnection((err, conn) => {
        const data = {
            addDate: Date.now(),
            updateDate: Date.now(),
            position_id: req.body.position_id,
            emp_id:req.body.emp_id
        }
        console.log(data);
        const sql = `INSERT INTO positionemployee  VALUES ('${data.addDate}','${data.updateDate}','${null}','${data.emp}','${data.position_id}')`;
        conn.query(sql, function (err, result) {
            if (err) throw err;
            console.log("1 record inserted");
            res.redirect("/hrposition_emp?id="+data.emp_id)
        });
    });
};

controller.delete = (req, res) => {
    req.getConnection((err, conn) => {
        id = req.query.id;
        emp_id = req.query.emp_id;
        console.log(id);
        sql = `DELETE FROM positionemployee WHERE position_emp_id='${id}'`;
        conn.query(sql, function (err, result) {
            if (err) throw err;
            console.log("1 delete inserted");
            res.redirect("/hrposition_emp?id="+emp_id)
        });
    });
}

module.exports = controller;